<h1>Description</h1>

The BreadcrumbNavigator QControl is a QControl and requires the QControl Toolkit: http://bit.ly/QControlNITools.  It inherits from and extends the String control.  It acts like a list of HTML-Like links to help provide navigation in an application.

Add items through an included property.  Clear all items through an included method.

When an item is clicked on, any thing after it is removed from the list and the clicked item is returned.

<h1>LabVIEW Version</h1>

This code is currently published in LabVIEW 2015.

<h1>Build Instructions</h1>

The <b>BreadcrumbNavigator.lvclass</b> and all its members can be distributed as a Class Library and built into the using application.

<h1>Installation Guide</h1>

The <b>BreadcrumbNavigator.lvclass</b> and all its members can be distributed as a Class Library and built into the using application.

<h1>Execution</h1>
See documentation distributed with the QControl Toolkit.

<h1>Support</h1>
Submit Issues or Merge Requests through GitLab.
